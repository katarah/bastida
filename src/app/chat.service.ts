import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(public http: HttpClient) { }
  baseURl = 'https://crudpi.io/c50bc8/feed';

  postFeed(usernme, msg) {
    const data = {
      username: usernme,
      message: msg
    };

    return this.http.post(this.baseURl, data);
  }
  getFeed() {
    return this.http.get(this.baseURl);
  }

}
